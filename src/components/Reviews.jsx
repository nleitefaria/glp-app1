import React from 'react';
import CustomBreadcrumb from './CustomBreadcrumb';

export default function Reviews(props) {
  return (            
	<div>
		<CustomBreadcrumb location="Reviews"></CustomBreadcrumb>
		Reviews
	</div>                      
  );
}