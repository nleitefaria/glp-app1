import React, { Component } from 'react';
import { NavLink } from "react-router-dom";

class Home extends Component {
	  render() {
		  return (
			      <div>	  		          
			      <br></br>
			      <h2>This project is a client for the <a href="https://hrk-app1.herokuapp.com/swagger-ui.html" rel="noopener noreferrer" target="_blank">Movies (MongoDB sample DB) API</a>.</h2>
			      <br></br>	
			      	<div>        
	              	<h3>Please browse the following subjects to get more info:</h3><br></br>
	              	</div>
	              	<div>  	
	            	- <NavLink to="/films">Films</NavLink>
	            	</div>
	            	<div>  
	            	- <NavLink to="/reviews">Reviews</NavLink>
	            	</div>            																		            					      					   
			      </div>
			    );
		}	 
}
	 
export default Home;