import React from 'react';
import { Route, NavLink, HashRouter } from "react-router-dom";
import { Container, Row, Col } from 'reactstrap';
import Home from './Home';
import Films from './Films';
import Reviews from './Reviews';

export default function Main(props) {
  return (            
	<HashRouter>	         
       <div style={{width:'80%', margin: 'auto'}}>               
         <div>
	     	<Container>
	        	<Row>
	            	<Col>
	            		<ul className="header">
	            			<li><NavLink to="/">Home</NavLink></li>
	            			<li><NavLink to="/films">Films</NavLink></li>
	            			<li><NavLink to="/reviews">Reviews</NavLink></li>	            																	
	            		</ul>
	            		<div className="content">
	            			<Route exact path="/" component={Home}/>
            				<Route exact path="/films" component={Films}/>
            				<Route exact path="/reviews" component={Reviews}/>   	            						
	            		</div>
	            	</Col>
	            </Row>
	        </Container>
	      </div>
      </div>             
    </HashRouter>                       
  );
}