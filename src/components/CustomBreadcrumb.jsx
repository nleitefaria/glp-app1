import React from 'react';
import { Breadcrumb, BreadcrumbItem } from 'reactstrap';
import { NavLink } from "react-router-dom";

export default function CustomBreadcrumb(props) {
  return (            
	<div>		
      <Breadcrumb>
        <BreadcrumbItem><NavLink to="/">Home</NavLink></BreadcrumbItem>
        <BreadcrumbItem active>{props.location}</BreadcrumbItem>
      </Breadcrumb>		
	</div>                      
  );
}