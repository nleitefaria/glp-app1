import React, { useState, useEffect } from 'react';
import axios from "axios";
import {
  PagingState,
  IntegratedPaging,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  PagingPanel,
} from '@devexpress/dx-react-grid-bootstrap4';
import CustomBreadcrumb from './CustomBreadcrumb';

export default function Films(props) 
{
  	const [columns] = useState(
    	[
        	{ name: 'title', title: 'Title' },
        	{ name: 'year', title: 'Year' }
      	]
  	);
  	
	const [rows, setRows] = useState([]);
  
  	useEffect(() => {
    	axios.get("https://hrk-app1.herokuapp.com/api/video/movies")
      		 .then(result => setRows(result.data));
  	}, []);

  	return (            
		<div>
			<CustomBreadcrumb location="Films"></CustomBreadcrumb>					
				<div className="card">
        			<Grid rows={rows} columns={columns}>
          				<PagingState defaultCurrentPage={0} pageSize={10} />
          					<IntegratedPaging />
          						<Table />
          							<TableHeaderRow />
          								<PagingPanel />
        			</Grid>
      			</div>         
		</div>                      
  );
}